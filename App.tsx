import React from 'react';
import { Text } from 'react-native';
import { WebView } from 'react-native-webview';

const App = () =>  {
    return (
      <WebView
        source={{ uri: 'https://prueba1.promo.ec/#/' }}
        style={{ marginTop: 20 }}
      />
    );
}

export default App